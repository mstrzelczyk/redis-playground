Simple Redis playground with Docker
=====
Prerequisites
-------

    - Linux with Docker installed (tested on v. 0.11.1 and 1.1.2)
    
How to play with Redis?
-------

    1. Build Docker image:
    
      docker build -t redis-playground .
      
    2. Run first container and Redis server:
    
        docker run -p 6379:6379 -i -t redis-playground /bin/bash
        
        ./redis-stable/src/redis-server
        
        (port forwarding needed for access to Redis server from other containers)
        
    3. Run another container and play with Redis commandline tool
    
        Check DOCKER_NETWORK_INTERFACE_IP - you can use ifconfig, docker's interface will probably be called docker0.
        When get the IP:
    
        docker run  -i -t redis-playground /bin/bash
        ./redis-stable/src/redis-cli -h DOCKER_NETWORK_INTERFACE_IP
        
        Now you are ready to use Redis commands:
            
            SET key value
            GET key
            
            etc.
        
    4. Check ruby scripts in /ruby dir when inside the container to see how pub/sub pattern works in Redis!        