FROM ubuntu:14.04

RUN \
  sudo apt-get update && \
  sudo apt-get -y install build-essential zlib1g-dev libssl-dev libreadline6-dev libyaml-dev && \
  apt-get -y install wget && \
  wget http://download.redis.io/redis-stable.tar.gz && \
  tar xvzf redis-stable.tar.gz && \
  cd redis-stable && \
  make && \
  cd .. && \
  wget http://cache.ruby-lang.org/pub/ruby/2.0/ruby-2.0.0-p481.tar.gz && \
  tar -xvzf ruby-2.0.0-p481.tar.gz && \
  cd ruby-2.0.0-p481/ && \
  ./configure --prefix=/usr/local && \
  make && \
  make install && \
  gem install redis

ADD ruby /ruby
