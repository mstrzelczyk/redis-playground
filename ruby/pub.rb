require 'redis'

if ARGV.count < 2
  puts "Run with 2 parameters: ruby pub.rb [REDIS HOST] [CHANNEL]"
  exit
end

host = ARGV[0]
channel = ARGV[1]

puts "Will publish to Redis on " + host + ", channel: " + channel

redis = Redis.new(:host => host)

trap(:INT) { puts; exit }

loop do
  print "TYPE A MESSAGE: "
  msg = STDIN.gets
  redis.publish channel, msg
end