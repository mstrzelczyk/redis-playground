require 'redis'

if ARGV.count < 2
  puts "Run with 2 parameters: ruby sub.rb [REDIS HOST] [CHANNEL]"
  exit
end

host = ARGV[0]
channel = ARGV[1]

trap(:INT) { puts; exit }

redis = Redis.new(:host => host, :timeout => 0)

redis.subscribe(channel) do |on|
  on.subscribe do |c, s|
    puts "SUBSCRIBED TO #{channel}"
  end
  
  on.message do |channel, msg|
    puts "MESSAGE: #{msg}"
  end
end
